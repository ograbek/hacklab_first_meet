#namespace dla taska, pozwala na logiczne grupowanie tasków i uniknięcie kolizji nazw
namespace :utils do

  # opis, ktory opisuje co task robi
  desc "Delete all files in /test directory"

  # symbol - nazwa taska
  task :clean_test_dir do

    #iterujemy po plikach w danej lokacji
    Dir["test/*"].each do |f|
      # jeżeli coś nie jest plikiem, pomijamy to
      next unless File.file?(f)

      # pytamy czy chcemy usunąć plik
      puts "Delete #{f} ?"

      # pobieramy odpowiedź z output-u
      answer = $stdin.gets

      # sprawdzamy odpowiedź
      case answer
      when /^y/
        #usuwamy plik
        File.unlink(f)
      when /^(n | q)/
        break
      end   
    end
  end

  

  desc "Copy files from /test directory to given directory"

  task :copy_from_test_dir do
    
    print "Where to copy files: "
    desc_dir = $stdin.gets.chomp  

    unless File.directory?(desc_dir)
      FileUtils.mkdir_p desc_dir
    end

    FileUtils.cp_r(Dir["test/*"], desc_dir)
  end

end