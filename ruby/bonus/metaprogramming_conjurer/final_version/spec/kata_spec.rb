require "spec_helper"

describe Conjurer do
      
  it "defines given as block method" do
    Conjurer.conjure(:one_through_five, ->{(1..5).to_a})
    Conjurer.new.one_through_five.must_equal [1,2,3,4,5]
  end

end