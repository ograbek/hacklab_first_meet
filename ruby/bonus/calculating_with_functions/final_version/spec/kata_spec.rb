require "spec_helper"

describe Kernel do
  
  it "calculates correctly " do
    seven(times(five)).must_equal 35
    four(plus(nine)).must_equal 13
    eight(minus(three)).must_equal 5       
    six(divided_by(two)).must_equal 3       
  end  

end