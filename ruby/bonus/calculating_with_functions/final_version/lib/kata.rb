# This time we want to write calculations using functions and get the results. Let's have a look at some examples:
#
# seven(times(five)) # must return 35
# four(plus(nine)) # must return 13
# eight(minus(three)) # must return 5
# six(divided_by(two)) # must return 3
#
# Requirements:
# There must be a function for each number from 0 ("zero") to 9 ("nine")
# There must be a function for each of the following mathematical operations: plus, minus, times, divided_by
# Each calculation consist of exactly one operation and two numbers
# The most outer function represents the left operand, the most inner function represents the right operand

{ zero: 0, one: 1, two: 2, three: 3, four: 4, five: 5, six: 6, seven: 7, eight: 8, nine: 9 }.each do |name, number|
  Object.send(:define_method, name) do |argument = nil|
    argument ? argument.call(number) : number
  end
end

{ plus: :+, minus: :-, times: :*, divided_by: :/ }.each do |name, operator|
  Object.send(:define_method, name) do |argument|
    Proc.new { |number| number.send(operator, argument*1.0) }
  end
end