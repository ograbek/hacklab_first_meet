require "spec_helper"

describe Kata do
  
  before do
    @kata = Kata.new
  end

  describe "#to_underscore" do
      
    it "changes camel case into snake case" do
      @kata.to_underscore('TestController').must_equal 'test_controller'
      @kata.to_underscore('ThisIsBeautifulDay').must_equal 'this_is_beautiful_day'
      @kata.to_underscore('Am7Days').must_equal 'am7_days'
      @kata.to_underscore('My3CodeIs4TimesBetter').must_equal 'my3_code_is4_times_better'
      @kata.to_underscore(5).must_equal '5'
    end

  end

end