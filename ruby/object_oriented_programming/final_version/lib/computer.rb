class Computer

  attr_accessor :is_on_sale, :price

  def initialize(price=100)
    @price = price
    @is_on_sale = false
  end
  
  def price
    is_on_sale ? @price/2.0 : @price
  end

  def toggle_sale
    self.is_on_sale = !is_on_sale    
  end

end