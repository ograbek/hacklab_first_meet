require "spec_helper"

describe Computer do

  before do
    @computer = Computer.new
  end

  describe "#price" do
    
    describe "when no price what set during initialization" do
      
      it "returns default price" do
        @computer.price.must_equal 100  
      end

    end

    describe "when price was set during initialization" do
      
      it "returns price that was set" do
        @computer = Computer.new(200)
        @computer.price.must_equal 200   
      end

    end

    describe "when computer was put on sale" do
      
      it "returns half of the initial price" do
        @computer = Computer.new(200)
        @computer.toggle_sale
        @computer.price.must_equal 100
      end

    end

  end

  describe "#price=" do
    
    it "sets computer price" do
      @computer = Computer.new(200)
      @computer.price.must_equal 200
      @computer.price = 150
      @computer.price.must_equal 150  
    end

  end

  describe "#is_on_sale?" do
    
    it "as default returns false" do
      @computer.is_on_sale.must_equal false
    end

    describe "when computer was put on sale" do
      
      it "returns true" do
        @computer.toggle_sale
        @computer.is_on_sale.must_equal true
      end

    end

  end

  describe "#toggle_sale" do
    
    it "toggle on sale state" do
      @computer.is_on_sale.must_equal false
      @computer.toggle_sale
      @computer.is_on_sale.must_equal true  
      @computer.toggle_sale
      @computer.is_on_sale.must_equal false  
    end

  end

end