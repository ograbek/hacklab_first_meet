require "spec_helper"

describe Kata do
  
  before do
    @kata = Kata.new
  end

  describe "#solution" do
    
    it "returns the url with anything after the anchor (#) removed" do
      @kata.solution('www.codewars.com#about').must_equal 'www.codewars.com'
      @kata.solution('www.codewars.com?page=1').must_equal 'www.codewars.com?page=1'
    end

  end

end