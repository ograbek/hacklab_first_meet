class Kata
  # Complete the function/method so that it returns the url with anything after the anchor (#) removed.
  # === Example
  #       remove_url_anchor('www.codewars.com#about') # returns 'www.codewars.com'
  #       remove_url_anchor('www.codewars.com?page=1') # returns 'www.codewars.com?page=1'

  def solution(url)
    url.gsub(/#.+$/, '')  
  end
  
end
