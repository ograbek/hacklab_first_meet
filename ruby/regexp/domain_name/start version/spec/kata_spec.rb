require "spec_helper"

describe Kata do
  
  before do
    @kata = Kata.new
  end

  describe "#solution" do
    
    it "returns domain for given url" do
      @kata.solution("http://github.com/carbonfive/raygun").must_equal "github" 
      @kata.solution("http://www.zombie-bites.com").must_equal "zombie-bites"
      @kata.solution("https://www.cnet.com").must_equal "cnet"  
    end

  end

end