class Kata
  # Write a function that when given a URL as a string, parses out just the domain 
  # name and returns it as a string. For example:
  # === Example
  #       solution("http://github.com/carbonfive/raygun") == "github" 
  #       solution("http://www.zombie-bites.com") == "zombie-bites"
  #       solution("https://www.cnet.com") == "cnet"

  def solution(url)
    url.match(/http[s]*:\/\/[www\.]*(.[^\.]+)/)[1]  
  end
  
end
