class Kata
  # Finish the solution so that it takes an input 'n' (integer) and returns a string that is
  # the decimal representation of the number grouped by commas after every 3 digits.
  # === Example
  #       solution(10) # should return "10"
  #       solution(100) # should return "100"
  #       solution(100) # should return "1,000"
  #       solution(35235235) # should return "35,235,235"

  def solution(number)
    number.to_s.reverse.scan(/\d{1,3}/).join(',').reverse 
  end
  
end
