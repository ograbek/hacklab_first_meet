require "spec_helper"

describe Kata do
  
  before do
    @kata = Kata.new
  end

  describe "#solution" do
    
    it "returns string representation of the number grouped by commas after every 3 digits" do
      @kata.solution(10).must_equal "10"
      @kata.solution(100).must_equal "100"
      @kata.solution(1000).must_equal "1,000"
      @kata.solution(35235235).must_equal "35,235,235"
    end

  end

end