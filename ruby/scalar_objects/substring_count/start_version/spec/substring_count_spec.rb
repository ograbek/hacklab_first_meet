require "spec_helper"

describe SubstringCount do
  
  before do
    @substr_count = SubstringCount.new
  end

  describe "#solution" do
    
    it "returns how many times substring is found within the given text" do
      @substr_count.solution('aa_bb_cc_dd_bb_e', 'bb').must_equal 2 
      @substr_count.solution('aaabbbcccc', 'bbb').must_equal 1  
    end

  end

end