require "spec_helper"

describe Kata do
  
  before do
    @kata = Kata.new
  end

  describe "#solution" do
    
    it "returns reversed string" do
      @kata.solution('world').must_equal 'dlrow'   
      @kata.solution('hello').must_equal 'olleh'  
      @kata.solution(nil).must_equal nil
    end

  end

end