class Kata
  
  # Complete the solution so that it reverses the string value passed into it.
  # === Example
  #       solution('world') # returns 'dlrow'
  #       solution('hello') # returns 'olleh'
  # 
  def solution(text)
    text.reverse unless text.nil?
  end

end