require "spec_helper"

describe Kata do
  
  before do
    @kata = Kata.new
  end

  describe "#solution" do
    
    it "returns only even values" do
      @kata.solution([1,2,3,4]).must_equal [2,4] 
    end

  end

end