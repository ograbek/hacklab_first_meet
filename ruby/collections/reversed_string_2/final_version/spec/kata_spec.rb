require "spec_helper"

describe Kata do
  
  before do
    @kata = Kata.new
  end

  describe "#solution" do
    
    it "returns how many times substring is found within the given text" do
      @kata.solution('world').must_equal 'dlrow'   
      @kata.solution('hello').must_equal 'olleh'  
      @kata.solution(nil).must_equal nil
    end

  end

end