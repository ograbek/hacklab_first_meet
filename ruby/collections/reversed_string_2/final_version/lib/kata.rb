class Kata
  # Complete the solution so that it reverses the string value passed into it, but without using build-in method reversed!
  # === Example
  #       solution('world') # returns 'dlrow'
  #       solution('hello') # returns 'olleh'

  def solution(text)
    unless text.nil?
      text.chars.each_with_object([]) do |el, new_str|
        new_str.unshift(el) 
      end.join('')
    end
  end
  
end
