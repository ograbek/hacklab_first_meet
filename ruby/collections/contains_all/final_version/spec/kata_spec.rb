require "spec_helper"

describe Array do
  
  before do
    @kata = [1,2,3,4,5,6]
  end

  describe "#contains_all?" do
    
    describe "when array contains all given elements" do
      
      it "returns true" do
        @kata.contains_all?([1,2,3]).must_equal true
      end

    end

    describe "when array doesn't contain all given elements" do
      
      it "returns false" do
        @kata.contains_all?([1,2,8]).must_equal false  
      end

    end

  end

end