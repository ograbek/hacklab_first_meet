require "spec_helper"

describe Kata do
  
  before do
    @kata = Kata.new
  end

  describe "#solution" do
    
    it "returns sum of squers" do
      @kata.solution([1,2, 2]).must_equal 9
    end

  end

end