class Kata
  # Complete the squareSum method so that it squares each number passed into it and then sums the results together.
  # === Example
  #       solution([1,2, 2]) # should return 9

  def solution(numbers)
    numbers.map {|n| n*n}.reduce(:+) 
  end
  
end
